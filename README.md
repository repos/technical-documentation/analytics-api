# Analytics API Documentation Site

Status: release candidate

## Running locally

Before you start: `npm install -g vitepress && npm install`

To develop: `vitepress dev`.

To build: `vitepress build`.

To preview build results: `vitepress preview`.

Note that the CI jobs run on NodeJS version 20 and might break with package-lock.json generated with a newer version.

---

## License

For Markdown files: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en).

For code and configuration files: [MIT-0](https://opensource.org/license/mit-0).

## Dependencies

In [DEPENDENCIES.md](/DEPENDENCIES.md).
