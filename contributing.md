---
title: Contributing
description: Contribute to the Analytics API docs.
---

# Contributing

We welcome feedback and contributions to the docs! Analytics API documentation is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) (content) and [MIT-0](https://opensource.org/license/mit-0) (code and configuration).

## Share feedback

To share feedback about the docs site or to report an issue with the docs, [create an account on Wikimedia Phabricator](https://www.mediawiki.org/wiki/Special:MyLanguage/Phabricator/Help#Creating_your_account), and [create a task](https://phabricator.wikimedia.org/maniphest/task/edit/form/1/?projects=data-products,Documentation).

## Submit a change

The source code for the docs site is stored in the [analytics-api repository](https://gitlab.wikimedia.org/repos/generated-data-platform/aqs/analytics-api) on Wikimedia GitLab.

To submit a merge request, [create an account](https://www.mediawiki.org/wiki/GitLab/Workflows/Registering_an_account_on_GitLab) on Wikimedia GitLab, and see the documentation for [creating a merge request](https://www.mediawiki.org/wiki/GitLab/Workflows/Making_a_merge_request).

## Add an API

To add documentation for a new Analytics Query Service (AQS) API, see the docs on Wikitech for [setting up docs for a new service](https://wikitech.wikimedia.org/wiki/AQS_2.0#Setting_up_docs_for_a_new_service).

## Acknowledgements

Some of the content on this site was copied or adapted from pages on [MediaWiki.org](https://mediawiki.org), [Wikitech](https://wikitech.wikimedia.org/), and the [Wikimedia API Portal](https://api.wikimedia.org/) licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en).

For information on copyrights and licenses of code dependencies used to build this website, see [DEPENDENCIES.md](https://gitlab.wikimedia.org/repos/generated-data-platform/aqs/analytics-api/-/blob/main/DEPENDENCIES.md?ref_type=heads).

### Images

- Wikipedia Pageviews Analysis - coffee versus tea - English Wikipedia by Wikimedia community, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Pageviews_Analysis_-_coffee_versus_tea.png). Cropped from original.
- Wikistats 2.0 Main by Nuria and Btullis, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), via [Wikitech](https://wikitech.wikimedia.org/wiki/File:Wikistats_2.0_Main.png). Cropped from original.
- Wikimedia logo by Neolux, Public domain, via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Wikimedia-logo_black.svg).
