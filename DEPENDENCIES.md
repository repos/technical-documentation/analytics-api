# Dependency copyrights and licenses

## ESLint

Copyright OpenJS Foundation and other contributors, <www.openjsf.org>

[MIT](https://github.com/eslint/eslint/blob/v8.57.0/LICENSE)

## eslint-plugin-vue

Copyright (c) 2017 Toru Nagashima

[MIT](https://github.com/vuejs/eslint-plugin-vue/blob/v9.26.0/LICENSE)

## Matomo Vitepress Plugin

Copyright (c) 2023 DINUM

[MIT](https://github.com/datagouv/vitepress-plugin-matomo/blob/v1.0.5/LICENSE)

## RapiDoc

Copyright (c) 2022 Mrinmoy Majumdar

[MIT](https://github.com/rapi-doc/RapiDoc/blob/v9.3.4/LICENSE.txt)

## Typescript

[Apache 2.0](https://github.com/microsoft/TypeScript/blob/v5.5.2/LICENSE.txt)

## Vitepress

Copyright (c) 2019-present, Yuxi (Evan) You

[MIT](https://github.com/vuejs/vitepress/blob/v1.2.3/LICENSE)

## Wikimedia ESLint config

Copyright (c) Oleg Gaidarenko <markelog@gmail.com>

[MIT](https://github.com/wikimedia/eslint-config-wikimedia/blob/v0.28.2/license)
