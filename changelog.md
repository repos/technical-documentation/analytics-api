---
title: Changelog
description: See the latest updates to the Analytics API.
---

# Changelog

See the latest updates to the Analytics API.

## 2024-07-17

Added [Commons analytics](/reference/commons) endpoints to provide data about categories and media files on Wikimedia Commons.

## 2023-08-01: Detail property type

Fixed a bug in the error response object for [device analytics](/reference/devices) that returned the `detail` property as an array instead of a string.

## 2020-04-29: Automated agent type

Added the `automated` agent type for page views identified as bots based on a set of heuristics, as opposed to the `spider` agent type that identifies bots based on user agent. The new value is available as a filter in the page views endpoints, and also affects the top metrics since bots-created artifacts will be less present.

## 2016-04-08: Bug fixes

Stripped out 'www' if it's passed into the project parameter. This is confusing when people try to look up www.mediawiki. Fixed decoding bug where articles with % in their titles were causing a 500 error. Fixed the date range to include start and end in the results.

## 2016-03-09: Top page views fix

Removed "-" from the top page views. The "-" page is both a redirect to the [Hyphen-minus](https://en.wikipedia.org/wiki/Hyphen-minus) page, and the way the Analytics team flags pages with unknown titles (such as search pages, diff pages, and other action specific pages). Community globally asked for this title to be removed from the top list.

## 2015-11-01: Initial release

Released three endpoints for page views: per-article, aggregate, and top. Some endpoints do not support all granularities yet.
