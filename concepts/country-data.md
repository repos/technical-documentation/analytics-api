---
title: Country data
description: Learn about how Wikimedia provides country data while protecting the privacy of users.
---

# Country data

Data related to geographical location of contributors has many privacy concerns. To minimize the risk to individuals resulting from publication of data, the Wikimedia Foundation follows the [Data Publication Guidelines](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines) and maintains the [Country and Territory Protection List](https://foundation.wikimedia.org/wiki/Legal:Country_and_Territory_Protection_List).

## Editors

To protect the privacy of editors, the Analytics API applies certain limitations on editor data split by country:

* The API provides ranges instead of exact numbers of editors per project and per country. For example, instead of returning the number of Romanian editors of Estonian Wikipedia, the API returns a range - between one and ten.
* The API doesn't provide editor data for wikis with fewer than three active editors in a given month. Wikis with three or more editors making five or more edits are included.

For more information, see the documentation for the [Geoeditors Monthly dataset](https://wikitech.wikimedia.org/wiki/Data_Platform/Data_Lake/Edits/Geoeditors/Public).

## Page views

To protect the privacy of readers, the Analytics API also limits page view data split by country:

* The API doesn't report the exact number of page views per country. Page view values are given in a bucketed format: Instead of reporting 7,876,451 views, the API returns `"views": "1000000-9999999"`. However, even though two or more countries might appear to have the same views in terms of buckets, the `rank` field reveals which country has more views.
* The API only returns page view data for countries with more than 100 page views in the requested time period. If no countries meet that threshold for a project, the project's per-country page views aren't reported by the API.
* The API doesn't report page view values of zero. See [Missing values within time series](/documentation/troubleshooting#missing-values-within-time-series).

For more information, see the documentation for [Page views per project](https://wikitech.wikimedia.org/wiki/Data_Platform/AQS/Pageviews/Pageviews_per_project).
