---
title: Unique devices
description: Learn about how Wikimedia defines unique devices.
---

# Unique devices

Counting unique devices is a privacy-respecting, indirect way of counting readers of Wikimedia projects. While not perfect - a single reader can use more than one device to access wikis - it's a reliable proxy that's good enough for most purposes.

A device is counted based on a cookie that contains information about its last visit to Wikimedia projects:
* If that visit was in the previous month (which means the device hasn't yet visited this month), it increases the count of unique devices for today and for this month.
* If that visit was before today, but still this month, the visit increases the count of unique devices for today, but not for this month.
* If that visit was today, the device isn't added to the count of devices for today or for this month.

For a more detailed description of this mechanism, see the [docs on Meta-Wiki](https://meta.wikimedia.org/wiki/Special:MyLanguage/Research:Unique_devices).
