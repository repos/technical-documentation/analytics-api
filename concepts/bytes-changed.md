---
title: Bytes changed
description: Learn about how Wikimedia defines changes in page length.
---

# Bytes changed

[Edit analytics](/reference/edits) endpoints include information about net and absolute change in page length resulting from edits.

## Net change

Net change is a sum of all changes in page length in a given period. Negative change indicates content removed, positive change - content added to the page.

For example, if an edit adds 5 bytes and another edit removes 10 bytes, the net change is -5 bytes.

## Absolute change

Absolute change is a total number of bytes changed on a page in a given period. Regardless of whether they result from additions or subtractions, these changes are represented by positive numbers.

For example, if an edit adds 5 bytes and another edit removes 10 bytes, the absolute change is 15 bytes.
