---
title: Page views
description: Learn about how Wikimedia defines page views.
---

# Page views

A page view is a request for content of a page that receives a response of `200 OK` or `304 Not Modified` (a redirect to cached version of the page). The MIME type of such a request is `text/html` for web browsers, or `application/json` for mobile app requests.

Additionally, the following rules apply when identifying page views:
* API requests count as page views if they come from the mobile app.
* Most requests to pages in the `Special` namespace aren't counted as page views.
* Edits aren't considered page views.
* Redirects, particularly redirects based on alternate spellings of a page title, aren't counted as views of the actual article. For more information, see [Redirects](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Traffic/Pageviews/Redirects#Per_article_analyses).

For a complete definition of a page view and extra background information, see [Research:Page view](https://meta.wikimedia.org/wiki/Special:MyLanguage/Research:Page_view).

## Spiders and automated traffic

Page views associated with user agents that self-identify as bots receive the `spider` category.

Other automated traffic, identified as such based on a set of heuristics, receives the `automated` category instead.

To learn more about detection of automatic page views, see [BotDetection](https://wikitech.wikimedia.org/wiki/Data_Platform/Data_Lake/Traffic/BotDetection).
