---
title: Compare editor numbers, year over year
description: Learn how to use Python to load editor data and compare editor numbers.
outline: deep
---

# Compare editor numbers, year over year

This tutorial explains how to use Python to load editor data from the [editors endpoint](/reference/editors), process it, and display a diagram with a year-over-year comparison of editor numbers.

A diagram like this could be useful to you, for example, in an analysis of seasonal trends in editor numbers.

Most sections on this page contain Python code snippets without comments. The full script, with comments, is available at the [end of the page](#full-script).

## Prerequisites

<!--@include:./parts/prerequisites.md-->

## Setting up and requesting data from the API

Start by importing the libraries installed earlier:

* [Pyplot](https://matplotlib.org/stable/api/pyplot_summary.html#module-matplotlib.pyplot), provided by Matplotlib, will allow you to create a plot of editor numbers.
* [Pandas](https://pandas.pydata.org/pandas-docs/stable/index.html) will allow you to prepare the editor data for displaying.
* [Requests](https://requests.readthedocs.io/en/latest/) will allow you to request editor data from the API.

```py
import matplotlib.pyplot as plt
import pandas as pd
import requests as rq
```

Next, specify the parameters for the API request. This means setting:
* user agent, as described in the [access policy](/documentation/access-policy#user-agent-policy)
* request URL. The URL defines what data you will request. You might want to see the [documentation of the editors endpoint](/reference/editors) to understand how to construct it. In this example, you will request monthly editor numbers from the beginning of 2016 to the end of 2023.

```py
headers = {
    "User-Agent": "Wikimedia Analytics API Tutorial (<your wiki username>) compare-editor-numbers.py"
}

url = """https://wikimedia.org/api/rest_v1/metrics/editors/aggregate/\
en.wikipedia.org/user/content/all-activity-levels/\
monthly/20160101/20240101"""
```

Next, request the data and parse the JSON response sent by the API.

```py
response = rq.get(url, headers=headers)
editor_numbers = response.json()
```

## Preparing data for plotting

Data returned in the JSON response has a tree-like structure. For numerical calculations and comparisons, it's better to have data in the form of a table or matrix.

In Python, a common table-like structure for working with data is a DataFrame (or mutable table) available in the Pandas library.

The following code snippet prepares the data for displaying by:
1. Creating a DataFrame from records returned by the API. Note that the rows of data are available under the `["items"].[0].["results"]` path of the JSON response, where `[0]` represents the first element of the "items" list. You can preview the structure of the JSON response by opening the [request link](https://wikimedia.org/api/rest_v1/metrics/editors/aggregate/en.wikipedia.org/user/content/all-activity-levels/monthly/20160101/20240101) in your browser.
2. Convert the data in the `timestamp` column to datetime, a dedicated data type for time data. This is one way of extracting month and year information from the returned records.
3. Create new columns for month and year data based on the `timestamp` column.
4. Remove the `timestamp` column as it's no longer needed.
5. Pivot the table so that data in the DataFrame is indexed by month, with columns representing years. This makes it easier to perform a year-over-year analysis.

```py
editors_df = pd.DataFrame.from_records(editor_numbers["items"][0]["results"])
date = pd.to_datetime(editors_df["timestamp"])
editors_df["month"] = pd.DatetimeIndex(date).month
editors_df["year"] = pd.DatetimeIndex(date).year
editors_df = editors_df.drop(columns=["timestamp"])
editor_df = editors_df.pivot(index="month", columns="year", values="editors")
```

## Displaying the plot

With the DataFrame prepared, you can now display the data.

Start by specifying the plot style, in this case `bmh`. You can learn more about the available plot styles by reading [Matplotlib's style sheets reference](https://matplotlib.org/stable/gallery/style_sheets/style_sheets_reference.html).

```py
plt.style.use("bmh")
```

Create a set of subplots and prepare to display them as a single plot based on the DataFrame. The only parameter necessary in the `editor_df.plot()` call is `ax`, as it tells the plot function to use the set of subplots created earlier. You can experiment with the other parameters, but the values in the example are:
* subplots set to `False` to display plots for all years on the same diagram
* `figsize` set to `20,10` to make sure the plot is wide enough to read comfortably
* `colormap` set to `Accent`, which is one of the colormaps available in Matplotlib. Colormap defines the colors used to present data on the plot. For more information, see [Choosing colormaps in Matplotlib](https://matplotlib.org/stable/users/explain/colors/colormaps.html).

With the configuration in place, you can display the plot.

```py
fig, ax = plt.subplots()
editor_df.plot(
    subplots=False, figsize=(20, 10), ax=ax, colormap="Accent"
)
plt.show()
```

## Next steps

To better understand the libraries and data used in this tutorial, be sure to experiment with different parameters in function calls and the request URL.

To learn how to combine data from multiple endpoints to display a single diagram, read the [Compare page metrics](/tutorials/compare-page-metrics) tutorial.

To learn more about the ecosystem of Python tools and libraries used in data science, explore the links listed in [Useful resources](#useful-resources).

To see what other endpoints are available to you in the Analytics API, check the API reference pages listed in the menu.

## Full script

The full script should look like the following.

<<< ./code/compare-editor-numbers.py

## Useful resources

* [Python home page](https://www.python.org/)
  * [Python documentation](https://docs.python.org/3/)
* [Conda](https://docs.conda.io/projects/conda/en/stable), package and environment manager popular among data scientists. It's often used to install and manage Python and R packages.
* [Matplotlib documentation](https://matplotlib.org/)
  * [Pyplot](https://matplotlib.org/stable/api/pyplot_summary.html#module-matplotlib.pyplot)
  * [Colormaps](https://matplotlib.org/stable/users/explain/colors/colormaps.html)
  * [Style sheets reference](https://matplotlib.org/stable/gallery/style_sheets/style_sheets_reference.html)
* [Pandas documentation](https://pandas.pydata.org/pandas-docs/stable/index.html)
  * [DataFrame](https://pandas.pydata.org/docs/reference/frame.html)
* [Requests documentation](https://requests.readthedocs.io/en/latest/)
* [JupyterLab and Jupyter Noteboook home page](https://jupyter.org/)
* [PAWS](https://wikitech.wikimedia.org/wiki/PAWS), the Jupyter Notebook instance hosted by the Wikimedia Foundation
