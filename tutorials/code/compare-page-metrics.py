"""Displays viewership and edit data for a page."""

import matplotlib.pyplot as plt
import pandas as pd
import requests as rq

# Prepare and make request #

# Specify user agent
# Be sure to customize this variable according to the access policy
# before running this script
headers = {
    "User-Agent": "GitLab CI automated test (/generated-data-platform/aqs/analytics-api) compare-page-metrics.py",
}

# URL for absolute article size difference data
diff_url = """https://wikimedia.org/api/rest_v1/metrics/bytes-difference/\
absolute/per-page/en.wikipedia.org/Land/all-editor-types/\
daily/20220401/20221231"""

# URL for viewership data
view_url = """https://wikimedia.org/api/rest_v1/metrics/pageviews/\
per-article/en.wikipedia.org/all-access/all-agents/\
Land/daily/20220401/20221231"""

# URL for edit number data
edit_url = """
https://wikimedia.org/api/rest_v1/metrics/edits/\
per-page/en.wikipedia.org/Land/all-editor-types/\
daily/20220401/20221231"""

# Request all data from APIs and parse responses as JSON
diff_response = rq.get(diff_url, headers=headers).json()
view_response = rq.get(view_url, headers=headers).json()
edit_response = rq.get(edit_url, headers=headers).json()

# Create Pandas DataFrame for bytes difference data
diff_df = pd.DataFrame.from_records(diff_response["items"][0]["results"])

# Parse timestamp and use it as index
diff_df["timestamp"] = pd.to_datetime(diff_df["timestamp"])
diff_df = diff_df.set_index("timestamp")

# Create Pandas DataFrame for viewership data
view_df = pd.DataFrame.from_records(view_response["items"])

# Parse timestamp and use it as index
# Note that timestamps for page views are in a different format
# and do not include time zone information, this command
# sets time zone to UTC
view_df["timestamp"] = pd.to_datetime(
    view_df["timestamp"], format="%Y%m%d%H"
).dt.tz_localize("UTC")
view_df = view_df.set_index("timestamp")

# Remove unnecessary columns included in the page view response
view_df = view_df.drop(columns=["project", "article", "granularity", "access", "agent"])

# Create Pandas DataFrame for edit data
edit_df = pd.DataFrame.from_records(edit_response["items"][0]["results"])

# Parse timestamp and use it as index
edit_df["timestamp"] = pd.to_datetime(edit_df["timestamp"])
edit_df = edit_df.set_index("timestamp")

# Merge the three DataFrames into one based on timestamp
# Note that the joins are defined as "outer" to retain all timestamps even
# if they are missing from any DataFrame. Also note the `.fillna(0).astype(int)`
# which fills missing data with zeros interpreted as integers
r = pd.merge(diff_df, edit_df, on="timestamp", how="outer")
r = pd.merge(r, view_df, on="timestamp", how="outer").fillna(0).astype(int)

# Set plot style
plt.style.use("bmh")

# Create a subplot layout
# Note that these plots do not share the Y axis (value), but share the X axis (timestamp)
# This is because these subplots have very different values. For example, the number of
# edits would be completely invisible if the Y axis scale was optimized for page view data
fig, axes = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=False, figsize=(15,10))

# Configure the subplot for views
r["views"].plot(ax=axes[0], color="c", title="Views")
# Configure the subplot for edits
r["edits"].plot(ax=axes[1], color="m", title="Edits")
# Configure the subplot for absolute change in bytes
r["abs_bytes_diff"].plot(ax=axes[2], color="y", title="Absolute change (bytes)")

# Display all subplots
plt.show()
