
"""Displays the editors by year plot."""

import matplotlib.pyplot as plt
import pandas as pd
import requests as rq

# Prepare and make a request #

# Specify the user agent
# Be sure to customize this variable according to the access policy
# before running this script
headers = {
    "User-Agent": "GitLab CI automated test (/generated-data-platform/aqs/analytics-api) compare-editor-numbers.py"
}

# Define the request URL
url = """https://wikimedia.org/api/rest_v1/metrics/editors/aggregate/\
en.wikipedia.org/user/content/all-activity-levels/\
monthly/20160101/20240101"""

# Request data from the API
response = rq.get(url, headers=headers)
# Parse the JSON response
editor_numbers = response.json()

# Prepare data #

# Create a pandas DataFrame
editors_df = pd.DataFrame.from_records(editor_numbers["items"][0]["results"])
# Convert the string timestamp to datetime
date = pd.to_datetime(editors_df["timestamp"])
# Create a new column for months
editors_df["month"] = pd.DatetimeIndex(date).month
# Create a new column for years
editors_df["year"] = pd.DatetimeIndex(date).year
# Remove the timestamp column as it's not needed anymore
editors_df = editors_df.drop(columns=["timestamp"])
# Pivot the data frame
editor_df = editors_df.pivot(index="month", columns="year", values="editors")

# Display results #

# Set a plot style
plt.style.use("bmh")

# Create a subplot set
fig, ax = plt.subplots()
# Configure the plot for the DataFrame
editor_df.plot(subplots=False, figsize=(20, 10), ax=ax, colormap="Accent")
# Display the plot
plt.show()
