To follow this tutorial, you should be familiar with Python, have a basic understanding of data structures, and know what it means to make API requests.

If you aren't familiar with Python, you can learn about it from the [official website](https://python.org), [Python books available on Wikibooks](https://en.wikibooks.org/wiki/Subject:Python_programming_language), or other sources available online.

Before requesting data from the API, be sure to read the [access policy](/documentation/access-policy).

### Software

This tutorial requires that you have access to a Python development environment. This can be a system-wide Python installation, a virtual environment, or a Jupyter Notebook (such as [PAWS](https://wikitech.wikimedia.org/wiki/PAWS), the instance hosted by the Wikimedia Foundation).

To run the code on this page, you need Python version 3.9 or higher. You also need to install the following extra libraries:
* [Matplotlib](https://matplotlib.org/)
* [Pandas](https://pandas.pydata.org/pandas-docs/stable/index.html)
* [Requests](https://requests.readthedocs.io/en/latest/)

To install these libraries, run the following command (or similar, depending on your Python distribution):

```sh
pip install matplotlib pandas requests
```
