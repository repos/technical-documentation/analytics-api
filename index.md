---
title: Wikimedia Analytics API
outline: false
---

# Wikimedia Analytics API

Open access to data about Wikipedia and other Wikimedia projects

<a class="main-button" href="./documentation/getting-started.html">GET STARTED</a>

## Get data about page views, edits, and more

The Wikimedia Analytics API provides a set of metrics that you can use to understand how readers and editors interact with [Wikipedia](https://www.wikipedia.org/) and many other [Wikimedia](https://meta.wikimedia.org/wiki/Special:MyLanguage/Wikimedia_projects) free-knowledge projects. For example, you can use the API to:

- Get the number of devices that visited Wikipedia in a given month
- List the most-viewed or most-edited pages on Wikipedia
- Compare the number of editors that edited Wikipedia by country

## Featured apps

Discover apps built with the Wikimedia Analytics API.

<div id="app-gallery">
    <div class="gallery-item">
        <div class="gallery-item-heading">
            <a href="https://stats.wikimedia.org/">Wikimedia Statistics</a>
        </div>
        <div class="gallery-item-image">
            <img src="/assets/wikistats.png" alt="Graphs showing monthly page view and edit statistics for French Wikipedia" />
        </div>
    </div>
    <div class="gallery-item">
        <div class="gallery-item-heading">
            <a href="https://pageviews.wmcloud.org/">Pageviews Analysis</a>
        </div>
        <div class="gallery-item-image">
           <img src="/assets/pageviews.png" alt="A graph comparing daily page views for the Coffee and Tea articles on English Wikipedia" />
        </div>
    </div>
</div>

## Backed by the Analytics Query Service (AQS)

To learn about the services behind the API, visit the [docs on Wikitech](https://wikitech.wikimedia.org/wiki/AQS_2.0).
