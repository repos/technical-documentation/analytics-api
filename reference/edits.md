---
title: Edit analytics
description: Reference docs and sandbox for edit analytics
outline: deep
---

# Edit analytics

Edit analytics provides data about the number of edits and edited pages on Wikimedia projects.

## Edits

Data returned by these endpoints includes edits on [redirects](https://en.wikipedia.org/wiki/Wikipedia:Redirect).

### Get number of edits

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /edits/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}"/>

### Get number of edits to a page

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /edits/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}"/>

## Changes in page length

Data returned by these endpoints includes edits on [redirects](https://en.wikipedia.org/wiki/Wikipedia:Redirect).

### Get net change, in bytes

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /bytes-difference/net/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}"/>

### Get net change for a page, in bytes

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /bytes-difference/net/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}"/>

### Get absolute change, in bytes

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /bytes-difference/absolute/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}"/>

### Get absolute change for a page, in bytes

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /bytes-difference/absolute/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}"/>

## Edited pages

Data returned by these endpoints does not include edits on [redirects](https://en.wikipedia.org/wiki/Wikipedia:Redirect).


### Get number of new pages

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /edited-pages/new/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}"/>

### Get number of edited pages

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /edited-pages/aggregate/{project}/{editor-type}/{page-type}/{activity-level}/{granularity}/{start}/{end}"/>

### List most-edited pages by net difference in bytes

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /edited-pages/top-by-net-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}"/>

### List most-edited pages by number of bytes changed

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /edited-pages/top-by-absolute-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}"/>

### List most-edited pages by number of edits

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/edits/api-spec.json" path="get /edited-pages/top-by-edits/{project}/{editor-type}/{page-type}/{year}/{month}/{day}"/>
