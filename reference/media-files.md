---
title: Media file analytics
description: Reference docs and sandbox for media file analytics
---

# Media file analytics

Media file analytics provides data about requests for media files on Wikimedia projects.
These endpoints serve data starting on January 1, 2015.
For more information about the underlying dataset, see the [docs on Wikitech](https://wikitech.wikimedia.org/wiki/Data_Platform/Data_Lake/Traffic/Mediacounts).

## Media types
Media types are classified according to file extensions.

| Extensions | Media type |
| ---------- | ---------- |
| svg, png, tiff, tiff, jpeg, gif, xcf, webp, bmp | image
| mp3, ogg, oga, flac, wav, midi, midi | audio
| webm, ogv | video
| pdf, djvu, srt, txt | document
| (all other extensions) | other

## Filter limitations

The ability to split and filter by referrer and by agent type is only available for data starting in May 2019. Before that, referrer is only split into internal, external, and unknown.

## Get number of media requests

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/mediarequests/api-spec.json" path="get /mediarequests/aggregate/{referer}/{media-type}/{agent}/{granularity}/{start}/{end}"/>

## Get number of media requests for a file

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/mediarequests/api-spec.json" path="get /mediarequests/per-file/{referer}/{agent}/{file-path}/{granularity}/{start}/{end}"/>

## List most-requested files

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/mediarequests/api-spec.json" path="get /mediarequests/top/{referer}/{media-type}/{year}/{month}/{day}"/>
