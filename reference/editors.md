---
title: Editor analytics
description: Reference docs and sandbox for editor analytics
---

# Editor analytics

Editor analytics provides data about the number of editors and newly registered users of Wikimedia projects.
Data returned by these endpoints includes edits on [redirects](https://en.wikipedia.org/wiki/Wikipedia:Redirect).

## Get number of editors

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/api-spec.json" path="get /editors/aggregate/{project}/{editor-type}/{page-type}/{activity-level}/{granularity}/{start}/{end}"/>

## Get number of editors by country

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/by-country/api-spec.json" path="get /editors/by-country/{project}/{activity-level}/{year}/{month}"/>

## Get number of new users

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/api-spec.json" path="get /registered-users/new/{project}/{granularity}/{start}/{end}"/>

## List most-active editors by net difference in bytes

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/api-spec.json" path="get /editors/top-by-net-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}"/>

## List most-active editors by number of bytes changed

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/api-spec.json" path="get /editors/top-by-absolute-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}"/>

## List most-active editors by number of edits

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/api-spec.json" path="get /editors/top-by-edits/{project}/{editor-type}/{page-type}/{year}/{month}/{day}"/>
