---
title: Device analytics
description: Reference docs and sandbox for device analytics
---

# Device analytics

Device analytics provides data about the number of unique devices that access Wikimedia projects.

This endpoint only returns data for projects that have at least 1,000 unique devices for the requested time period.
Projects with less than 1,000 unique devices show too much random variation for the data to be actionable.
For more information, visit [Last access solution on Wikitech](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Traffic/Unique_Devices/Last_access_solution#How_did_we_determined_this_1000_number_for_the_per-domain_uniques).

## Get number of unique devices

<SpecView path="get /unique-devices" specUrl="https://wikimedia.org/api/rest_v1/metrics/unique-devices/api-spec.json" expanded="false"/>
