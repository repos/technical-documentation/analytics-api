---
title: Page view analytics
description: Reference docs and sandbox for page view analytics
---

# Page view analytics

Page view analytics provides data about page views for Wikimedia projects.
These endpoints serve data starting on July 1, 2015.

## Get number of page views

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" path="get /pageviews/aggregate/{project}/{access}/{agent}/{granularity}/{start}/{end}"/>

## Get number of page views by country

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" path="get /pageviews/top-by-country/{project}/{access}/{year}/{month}"/>

## Get number of page views for a page

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" path="get /pageviews/per-article/{project}/{access}/{agent}/{article}/{granularity}/{start}/{end}"/>

## List most-viewed pages

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" path="get /pageviews/top/{project}/{access}/{year}/{month}/{day}"/>

## List most-viewed pages for a country

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" path="get /pageviews/top-per-country/{country}/{access}/{year}/{month}/{day}"/>

## Get number of page views (legacy)

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" path="get /legacy/pagecounts/aggregate/{project}/{access-site}/{granularity}/{start}/{end}"/>
