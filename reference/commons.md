---
title: Commons analytics
description: Reference docs and sandbox for Commons analytics
---

# Commons analytics

Commons analytics provides data about the usage of categories and media files on [Wikimedia Commons](https://commons.wikimedia.org/wiki/Special:MyLanguage/Commons:Welcome). This data is focused on categories associated with contributions from galleries, libraries, archives, and museums (GLAM).

## Available data

Because of data size and complexity, Commons analytics data is only available for:

* categories on the [allow list](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics/dags/commons/commons_category_allow_list.tsv)
* subcategories of allowed categories, up to seven steps from the allowed category on the [category tree](https://commons.wikimedia.org/wiki/Special:CategoryTree)
* media files directly associated with any of these categories or subcategories

To learn more about the underlying dataset and how to add a category to the allow list, see [Commons Impact Metrics on Wikitech](https://wikitech.wikimedia.org/wiki/Commons_Impact_Metrics).

## Response properties

For details about response properties, see the [data model documentation on Wikitech](https://wikitech.wikimedia.org/wiki/Commons_Impact_Metrics/Data_Model).

## Categories

### Get time series of category metrics
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/category-metrics-snapshot/{category}/{start}/{end}"/>

### Get time series of edit counts for a given category
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/edits-per-category-monthly/{category}/{category-scope}/{edit-type}/{start}/{end}"/>

### Get time series of pageview counts for a given category
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/pageviews-per-category-monthly/{category}/{category-scope}/{wiki}/{start}/{end}"/>

### Get ranking of most edited categories
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-edited-categories-monthly/{category-scope}/{edit-type}/{year}/{month}"/>

### Get ranking of categories with most pageviews
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-viewed-categories-monthly/{category-scope}/{wiki}/{year}/{month}"/>

### Get ranking of wikis with most pageviews for a given category
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-wikis-per-category-monthly/{category}/{category-scope}/{year}/{month}"/>

### Get ranking of pages with most pageviews for a given category
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-pages-per-category-monthly/{category}/{category-scope}/{wiki}/{year}/{month}"/>

### Get ranking of users with most edits to a given category
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-editors-monthly/{category}/{category-scope}/{edit-type}/{year}/{month}"/>

## Media files

### Get time series of media file metrics
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/media-file-metrics-snapshot/{media-file}/{start}/{end}"/>

### Get time series of pageview counts for a given media file
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/pageviews-per-media-file-monthly/{media-file}/{wiki}/{start}/{end}"/>

### Get ranking of media files with most pageviews
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-viewed-media-files-monthly/{category}/{category-scope}/{wiki}/{year}/{month}"/>

### Get ranking of wikis with most pageviews for a given media file
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-wikis-per-media-file-monthly/{media-file}/{year}/{month}"/>

### Get ranking of pages with most pageviews for a given media file
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/top-pages-per-media-file-monthly/{media-file}/{wiki}/{year}/{month}"/>

## Users

### Get time series of edit counts for a given user
<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/commons-analytics/api-spec.json" path="get /commons-analytics/edits-per-user-monthly/{user-name}/{edit-type}/{start}/{end}"/>
