import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Wikimedia Analytics API",
  description: "Open access to data about Wikimedia projects",
  base: '/generated-data-platform/aqs/analytics-api/',
  srcExclude: ["**/README.md", "**/DEPENDENCIES.md", "**/tutorials/parts/*.md"],
  lastUpdated: true,
  head: [['meta', { "http-equiv": "Content-Security-Policy",
                    "content": "default-src 'self' wikimedia.org *.wikimedia.org data:; object-src 'none'; script-src blob: 'unsafe-inline' 'unsafe-eval' 'self' wikimedia.org piwik.wikimedia.org; style-src 'unsafe-inline' 'self'; connect-src 'self' wikimedia.org piwik.wikimedia.org;"}
                  ]],
    themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    editLink: {
      pattern: 'https://gitlab.wikimedia.org/repos/generated-data-platform/aqs/analytics-api/-/blob/main/:path?plain=1',
      text: "Edit this page on GitLab"
    },
    logo: {
      light: '/assets/wm-black.svg',
      dark: '/assets/wm-white.svg'
    },
    nav: [
      { text: 'Home', link: '/' }
    ],
    footer: {
      message: '<a href="https://foundation.wikimedia.org/wiki/Special:MyLanguage/Policy:Privacy_policy">Privacy Policy</a> | <a href="https://foundation.wikimedia.org/wiki/Special:MyLanguage/Policy:Terms_of_Use">Terms of Use</a> | <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en">Content: CC BY-SA 4.0</a> | <a href="https://opensource.org/license/mit-0">Code: MIT-0</a>',
      copyright: 'Copyright © Wikimedia Foundation and contributors'
    },
    search: {
      provider: 'local'
    },
    sidebar: [
      {
        text: 'Documentation',
        items: [
          { text: 'Getting started', link: '/documentation/getting-started' },
          { text: 'Access policy', link: '/documentation/access-policy' },
          { text: 'Stability policy', link: '/documentation/stability-policy' },
          { text: 'Clients', link: '/documentation/clients' },
          { text: 'Troubleshooting', link: '/documentation/troubleshooting' }
        ]
      },
      {
        text: 'Concepts',
        items: [
          { text: 'Page views', link: '/concepts/page-views' },
          { text: 'Unique devices', link: '/concepts/unique-devices' },
          { text: 'Bytes changed', link: '/concepts/bytes-changed' },
          { text: 'Country data', link: '/concepts/country-data' },
        ]
      },
      {
        text: 'Examples',
        items: [
          { text: 'Page metrics', link: '/examples/page-metrics' },
          { text: 'Project metrics', link: '/examples/project-metrics' },
        ]
      },

      {
        text: 'Tutorials',
        items: [
          { text: 'Compare editor numbers', link: '/tutorials/compare-editor-numbers'},
          { text: 'Compare page metrics', link: '/tutorials/compare-page-metrics' }
        ]
      },
       {
        text: 'API reference',
        items: [
          { text: 'Commons', link: '/reference/commons' },
          { text: 'Devices', link: '/reference/devices' },
          { text: 'Edits', link: '/reference/edits' },
          { text: 'Editors', link: '/reference/editors' },
          { text: 'Media files', link: '/reference/media-files' },
          { text: 'Page views', link: '/reference/page-views' },
        ]
      },
      {
        text: 'Contributing',
        link: '/contributing'
      },
      {
        text: 'Changelog',
        link: '/changelog'
      }
    ],
  }
})
