import './style.css'
import DefaultTheme from 'vitepress/theme-without-fonts'
import SpecView from '../../src/components/SpecView.vue'
import TryExample from '../../src/components/TryExample.vue'
import matomo from "@datagouv/vitepress-plugin-matomo";
import type { Theme } from 'vitepress'

export default {
  extends: DefaultTheme,
  enhanceApp({ app, router, siteData }) {
    matomo({
      router: router,
      siteID: 26,
      trackerUrl: "https://piwik.wikimedia.org/",
      trackerPhpFile: "matomo.php",
      trackerJsFile: "matomo.js"
    });
    app.component('SpecView', SpecView);
    app.component('TryExample', TryExample);
  }
} satisfies Theme
