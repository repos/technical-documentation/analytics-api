---
title: Page metrics
description: Get examples of API requests for metrics about individual wiki pages.
---

# Page metrics

This page includes examples of API requests for metrics about individual wiki pages.

## Page changes

### Net change in article length

Get the time series of daily net changes in article length for English Wikipedia's article "Jupiter" on all days in October 2023. Only count changes made by registered users that are not bots.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/bytes-difference/net/per-page/en.wikipedia.org/Jupiter/user/daily/20231001/20231031" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/bytes-difference/net/per-page/en.wikipedia.org/Jupiter/user/daily/20231001/20231031" />

### Absolute change in article length

Get the time series of daily absolute changes (counted as sums of all changes) in article length for English Wikipedia's article "Jupiter" on all days in October 2023. Only count changes made by registered users that are not bots.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/bytes-difference/absolute/per-page/en.wikipedia.org/Jupiter/user/daily/20231001/20231031" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/bytes-difference/absolute/per-page/en.wikipedia.org/Jupiter/user/daily/20231001/20231031" />

### Number of edits

Get the time series of numbers of edits to English Wikipedia's article "Jupiter" in October 2023. Only count changes made by registered users that are not bots.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/edits/per-page/en.wikipedia.org/Jupiter/user/daily/20231001/20231031" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/edits/per-page/en.wikipedia.org/Jupiter/user/daily/20231001/20231031" />

## Page views

### In a month

Get the time series of numbers of page views on German Wikipedia's article "Jupiter (Planet)" in October 2023.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/de.wikipedia.org/all-access/all-agents/Jupiter%20(Planet)/daily/20231001/20231031" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/de.wikipedia.org/all-access/all-agents/Jupiter%20(Planet)/daily/20231001/20231031" />

### On a single day, in mobile app

Get the number of mobile application page views on French Wikipedia's article "Jupiter (planète)" on November 15, 2023.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/fr.wikipedia.org/mobile-app/all-agents/Jupiter%20(plan%C3%A8te)/daily/20231115/20231115" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/fr.wikipedia.org/mobile-app/all-agents/Jupiter%20(plan%C3%A8te)/daily/20231115/20231115" />
