---
title: Project metrics
description: Get examples of API requests for metrics about Wikimedia projects.
---

# Project metrics

This page includes examples of API requests for metrics about Wikimedia projects.

## Aggregated views

Get the time series of numbers of monthly automated page views for all Wikimedia projects in 2023.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/pageviews/aggregate/all-projects/all-access/automated/monthly/2023010100/2023123100" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/pageviews/aggregate/all-projects/all-access/automated/monthly/2023010100/2023123100" />

## Unique devices

### Daily, Spanish Wikipedia

Get the time series of numbers of unique devices that visited Spanish Wikipedia in February 2021.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/unique-devices/es.wikipedia.org/all-sites/daily/20210201/20210228" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/unique-devices/es.wikipedia.org/all-sites/daily/20210201/20210228" />

### Monthly, all Wikipedias

Get the time series of numbers of unique devices that visited all Wikipedias in the first three months of 2020.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/unique-devices/all-wikipedia-projects/all-sites/monthly/20200101/20200331" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/unique-devices/all-wikipedia-projects/all-sites/monthly/20200101/20200331" />

## Editors

Get the time series of numbers of editors who edited entries in Polish Wiktionary between 5 and 24 times in the months between January and August 2021.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/editors/aggregate/pl.wiktionary.org/all-editor-types/all-page-types/5..24-edits/monthly/20210101/20210901" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/editors/aggregate/pl.wiktionary.org/all-editor-types/all-page-types/5..24-edits/monthly/20210101/20210901" />

## Most-viewed pages

Get the list of top 1000 most-viewed articles on English Wikipedia in March 2023.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/pageviews/top/en.wikipedia.org/all-access/2023/03/all-days" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/pageviews/top/en.wikipedia.org/all-access/2023/03/all-days" />

## New pages

Get the time series of numbers of non-content pages created by all editor types on Spanish Wikipedia between January 10 and January 30, 2020.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/edited-pages/new/es.wikipedia.org/all-editor-types/non-content/daily/20200110/20200130" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/edited-pages/new/es.wikipedia.org/all-editor-types/non-content/daily/20200110/20200130" />

## Most-edited pages

Get the list of top 100 content pages with the most edits on Russian Wikipedia in January 2019.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/edited-pages/top-by-edits/ru.wikipedia.org/all-editor-types/content/2019/01/all-days" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/edited-pages/top-by-edits/ru.wikipedia.org/all-editor-types/content/2019/01/all-days" />

## Files

### Most-requested files

Get the list of most-requested media files in September 2023.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/mediarequests/top/all-referers/all-media-types/2023/09/all-days" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/mediarequests/top/all-referers/all-media-types/2023/09/all-days" />

### Numbers of file requests

Get the time series of numbers of media file requests in every month of 2022.

```sh
curl -X GET \
"https://wikimedia.org/api/rest_v1/metrics/mediarequests/aggregate/all-referers/all-media-types/all-agents/monthly/20220101/20230101" \
 -H "accept: application/json"
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/mediarequests/aggregate/all-referers/all-media-types/all-agents/monthly/20220101/20230101" />
