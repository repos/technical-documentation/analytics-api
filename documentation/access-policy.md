---
title: Access policy
description: Learn about rate limits, data licensing, and terms of use.
---

# Access policy

This page includes information to help you comply with Wikimedia's policies for using the Analytics API.

## Terms of use

By using the API, you agree to Wikimedia's [terms of use](https://foundation.wikimedia.org/wiki/Special:MyLanguage/Policy:Terms_of_Use) and [privacy policy](https://foundation.wikimedia.org/wiki/Special:MyLanguage/Policy:Privacy_policy).

## Data licensing

Data provided by the API is available under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/)

## Client identification

The API requires an HTTP [User-Agent header](https://en.wikipedia.org/wiki/User-Agent_header) for all requests.
This helps identify your app and ensures that system administrators can contact you if a problem arises.
Clients making requests without a User-Agent header may be blocked without notice.
See the Wikimedia Foundation's [User-Agent policy](https://foundation.wikimedia.org/wiki/Special:MyLanguage/Policy:User-Agent_policy) for more information.

The User-Agent header can include a link to a user page on a Wikimedia wiki, a URL for a relevant external website, or an email address.

```ansi
# Preferred format for User-Agent headers
<client name>/<version> (<contact information>) <library/framework name>/<version>
```

If you are calling the API from browser-based JavaScript, you may not be able to influence the User-Agent header, depending on the browser.
To work around this, use the `Api-User-Agent header`.

## Data downloads

Datasets provided via the API can also be downloaded in bulk via [dumps.wikimedia.org](https://dumps.wikimedia.org/other/analytics/).

## Rate limits

There's no fixed limit on requests to the API, but your client may be blocked if you endanger the stability of the service. To stay within a safe request rate, wait for each request to finish before sending another request.
