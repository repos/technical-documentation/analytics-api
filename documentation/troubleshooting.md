---
title: Troubleshooting
description: Find solutions to issues you may encounter when using the Analytics API.
---

# Troubleshooting

This page describes issues you may encounter when using the Analytics API.

## Data availability

Data is loaded at the end of the time span in question. For example:
* Data for 2015-12-01 was loaded on 2015-12-02 00:00:00 UTC.
* Data for 2015-11-10 18:00:00 UTC was loaded on 2015-11-10 19:00:00 UTC.

Data loads into the API from a large stream of data. This process is usually done within a few hours, but can take 24 hours or more if there are problems.
To be notified of any significant delays in loading data, subscribe to the [analytics mailing list](https://lists.wikimedia.org/postorius/lists/analytics.lists.wikimedia.org/).

## 404 means zero or not loaded

`404 not found` responses from the API can mean that there are `0` page views for the given project, time span and filters specified in the query. This error may also occur if your client requests data that hasn't been loaded into the API's database; see [data availability](#data-availability). This happens because, due to implementation reasons, the API can't distinguish between actual zeros and data that hasn't been loaded in the database.

## Missing values within time series

Because of the same caveat ([404 means zero or not loaded](#_404-means-zero-or-not-loaded)), zero values are omitted from API responses. This means that, if you request a time series from the API, there may be gaps in the time series in the response. Be aware that this can break charting libraries, and you'll need to account for the missing zeros. See [Compare page metrics](/tutorials/compare-page-metrics) for an example.

## 429 throttling

If you receive a `429 Too Many Requests` response from the API, your client has made too many requests and is being throttled. This will happen if the storage can't keep up with the request ratio from a given IP. Throttling is enforced at the storage layer, meaning that if you request data available in cache (because another client has requested it earlier), there is no throttling.

## High number of views for the "-" page

The dash value is used as a special value for "no page title found" when extracting titles from URLs, so a page titled "-" may appear to receive an unusually high number of page views.

## Issues with datasets

To learn about issues affecting the underlying datasets, see the corresponding documentation on Wikitech:

* [Devices](https://wikitech.wikimedia.org/wiki/Data_Platform/Data_Lake/Traffic/Unique_Devices#Changes_and_Known_Problems_with_Dataset)
* [Page views](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Traffic/Pageview_hourly#Changes_and_known_problems_since_2015-06-16)
