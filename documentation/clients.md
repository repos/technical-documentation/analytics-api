---
title: Clients
description: Browse client libraries for the Analytics API.
---

# Clients

This pages lists available client libraries for the Wikimedia Analytics API.

## JavaScript

* [pageviews.js](https://github.com/tomayac/pageviews.js) (page views and devices)

## Python

* [mwviews](https://github.com/mediawiki-utilities/python-mwviews) (page views)
* [pageviewapi](https://github.com/Commonists/pageview-api) (page views)

## R

* [waxer](https://github.com/wikimedia/waxer) (page views, devices, edits, editors, and media files)
* [pageviews](https://github.com/ironholds/pageviews) (page views)
