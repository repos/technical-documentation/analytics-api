---
title: Getting started
description: Learn about Wikimedia projects, and make your first request.
---

# Getting started

[Wikimedia projects](https://meta.wikimedia.org/wiki/Special:MyLanguage/Wikimedia_projects) are free, collaborative repositories of knowledge, written and maintained by volunteers around the world.
Each project hosts a different type of content, such as encyclopedia articles on [Wikipedia](https://www.wikipedia.org/) and dictionary entries on [Wiktionary](https://www.wiktionary.org/).
The Analytics API gives you open access to data about Wikimedia projects, including page views, unique device, and more.

## Projects and languages

Most Wikimedia projects are created and maintained in a single language.
This means that what we think of as Wikipedia is really over 300 different Wikipedias (English Wikipedia, Cebuano Wikipedia, Swedish Wikipedia, and many more) all with original articles in their own language.
Single-language projects use the language code as the subdomain.

Some projects are maintained in English and translated into other languages (like [Commons](https://commons.wikimedia.org/wiki/Special:MyLanguage/Main_Page)), or they are created to be language neutral (like [Wikispecies](https://species.wikimedia.org/wiki/Special:MyLanguage/Main_Page)).
These multilingual projects use the project name as the subdomain instead of a language code.

For a complete list of projects and languages, visit the [site matrix on Meta-Wiki](https://meta.wikimedia.org/wiki/Special:SiteMatrix).

## API overview

API endpoints share a consistent URL structure that begins with:

```
https://wikimedia.org/api/rest_v[version number]/metrics/
```

The API uses HTTP [request methods](https://en.wikipedia.org/wiki/HTTP#Request_methods) and [response status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) and returns data in [JSON](https://en.wikipedia.org/wiki/JSON) format.

## Make your first request

Get the number of monthly page views for English Wikipedia in 2023.

```sh
curl https://wikimedia.org/api/rest_v1/metrics/pageviews/aggregate/en.wikipedia.org/all-access/all-agents/monthly/2023010100/2024010100
```

<TryExample href="https://wikimedia.org/api/rest_v1/metrics/pageviews/aggregate/en.wikipedia.org/all-access/all-agents/monthly/2023010100/2024010100" />

## Try the sandbox

Click on the box below to open the sandbox for the **get number of page views** endpoint.

You can use the sandbox to explore the data by trying different parameters.
Change the `project` parameter to get page views for Japanese Wikipedia (`ja.wikipedia.org`), or try different `access` methods and `agent` types.

<div class="explanation">
    <div class="side start"></div>
    <div class="note">↓ CLICK TO EXPAND ↓</div>
    <div class="side end"></div>
</div>

<SpecView specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" path="get /pageviews/aggregate/{project}/{access}/{agent}/{granularity}/{start}/{end}" expanded="false"/>

<div class="explanation under">
    <div class="side start"></div>
    <div class="side end"></div>
</div>

## Next steps

- Read about [policies](access-policy) for data licensing and API rate limits.
- Discover the underlying **concepts** behind Wikimedia data, such as [page views](/concepts/page-views).
- Learn how to use the API with **examples** and **tutorials**, such as [comparing page metrics](/tutorials/compare-editor-numbers).
- Explore available endpoints in the **API reference**, and try the sandbox for each endpoint.

## Other Wikimedia APIs

Use the Analytics API alongside other APIs that allow you to interact with Wikimedia projects.

- Get wiki content and metadata with the [MediaWiki Action API](https://www.mediawiki.org/wiki/Special:MyLanguage/API:Main_page).
- Get daily featured content with the [Wikimedia REST API](https://www.mediawiki.org/wiki/Special:MyLanguage/Wikimedia_REST_API).
- Get the most recent changes to Wikimedia projects with the [Event Steams API](https://wikitech.wikimedia.org/wiki/Event_Platform/EventStreams_HTTP_Service).
