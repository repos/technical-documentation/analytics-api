---
title: Stability policy
description: Learn about API versioning and how to be notified about important changes.
---

# Stability policy

This page describes the versioning policy for the Wikimedia Analytics API.

## Versions

The API is versioned using a version number in the path (for example: `/v1/`).
Following the principles of [semantic versioning](https://semver.org/), the version number is incremented when an endpoint is changed in a backwards-incompatible way, such as removing a response property or adding a required parameter.
Within a major version, the API may change in backwards-compatible ways, such as adding a response property or optional request parameter.

## Updates

To be notified about important updates to the API, subscribe to the [analytics mailing list](https://lists.wikimedia.org/postorius/lists/analytics.lists.wikimedia.org/).
